<?php
/**
 * ShippingService
 *
 * PHP version 7.4
 *
 * @category Class
 * @package  MyBit\Ebay\Account
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Account API
 *
 * The <b>Account API</b> gives sellers the ability to configure their eBay seller accounts, including the seller's policies (eBay business policies and seller-defined custom policies), opt in and out of eBay seller programs, configure sales tax tables, and get account information.  <br><br>For details on the availability of the methods in this API, see <a href=\"/api-docs/sell/account/overview.html#requirements\">Account API requirements and restrictions</a>.
 *
 * The version of the OpenAPI document: v1.9.2
 * Generated by: https://openapi-generator.tech
 * Generator version: 7.6.0
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace MyBit\Ebay\Account\Model;

use \ArrayAccess;
use \MyBit\Ebay\Account\ObjectSerializer;

/**
 * ShippingService Class Doc Comment
 *
 * @category Class
 * @description This type is used by the &lt;b&gt;shippingServices&lt;/b&gt; array, an array that provides details about every domestic and international shipping service option that is defined for the policy.
 * @package  MyBit\Ebay\Account
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<string, mixed>
 */
class ShippingService implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'ShippingService';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'additional_shipping_cost' => '\MyBit\Ebay\Account\Model\Amount',
        'buyer_responsible_for_pickup' => 'bool',
        'buyer_responsible_for_shipping' => 'bool',
        'free_shipping' => 'bool',
        'shipping_carrier_code' => 'string',
        'shipping_cost' => '\MyBit\Ebay\Account\Model\Amount',
        'shipping_service_code' => 'string',
        'ship_to_locations' => '\MyBit\Ebay\Account\Model\RegionSet',
        'sort_order' => 'int',
        'surcharge' => '\MyBit\Ebay\Account\Model\Amount'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'additional_shipping_cost' => null,
        'buyer_responsible_for_pickup' => null,
        'buyer_responsible_for_shipping' => null,
        'free_shipping' => null,
        'shipping_carrier_code' => null,
        'shipping_cost' => null,
        'shipping_service_code' => null,
        'ship_to_locations' => null,
        'sort_order' => 'int32',
        'surcharge' => null
    ];

    /**
      * Array of nullable properties. Used for (de)serialization
      *
      * @var boolean[]
      */
    protected static array $openAPINullables = [
        'additional_shipping_cost' => false,
        'buyer_responsible_for_pickup' => false,
        'buyer_responsible_for_shipping' => false,
        'free_shipping' => false,
        'shipping_carrier_code' => false,
        'shipping_cost' => false,
        'shipping_service_code' => false,
        'ship_to_locations' => false,
        'sort_order' => false,
        'surcharge' => false
    ];

    /**
      * If a nullable field gets set to null, insert it here
      *
      * @var boolean[]
      */
    protected array $openAPINullablesSetToNull = [];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of nullable properties
     *
     * @return array
     */
    protected static function openAPINullables(): array
    {
        return self::$openAPINullables;
    }

    /**
     * Array of nullable field names deliberately set to null
     *
     * @return boolean[]
     */
    private function getOpenAPINullablesSetToNull(): array
    {
        return $this->openAPINullablesSetToNull;
    }

    /**
     * Setter - Array of nullable field names deliberately set to null
     *
     * @param boolean[] $openAPINullablesSetToNull
     */
    private function setOpenAPINullablesSetToNull(array $openAPINullablesSetToNull): void
    {
        $this->openAPINullablesSetToNull = $openAPINullablesSetToNull;
    }

    /**
     * Checks if a property is nullable
     *
     * @param string $property
     * @return bool
     */
    public static function isNullable(string $property): bool
    {
        return self::openAPINullables()[$property] ?? false;
    }

    /**
     * Checks if a nullable property is set to null.
     *
     * @param string $property
     * @return bool
     */
    public function isNullableSetToNull(string $property): bool
    {
        return in_array($property, $this->getOpenAPINullablesSetToNull(), true);
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'additional_shipping_cost' => 'additionalShippingCost',
        'buyer_responsible_for_pickup' => 'buyerResponsibleForPickup',
        'buyer_responsible_for_shipping' => 'buyerResponsibleForShipping',
        'free_shipping' => 'freeShipping',
        'shipping_carrier_code' => 'shippingCarrierCode',
        'shipping_cost' => 'shippingCost',
        'shipping_service_code' => 'shippingServiceCode',
        'ship_to_locations' => 'shipToLocations',
        'sort_order' => 'sortOrder',
        'surcharge' => 'surcharge'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'additional_shipping_cost' => 'setAdditionalShippingCost',
        'buyer_responsible_for_pickup' => 'setBuyerResponsibleForPickup',
        'buyer_responsible_for_shipping' => 'setBuyerResponsibleForShipping',
        'free_shipping' => 'setFreeShipping',
        'shipping_carrier_code' => 'setShippingCarrierCode',
        'shipping_cost' => 'setShippingCost',
        'shipping_service_code' => 'setShippingServiceCode',
        'ship_to_locations' => 'setShipToLocations',
        'sort_order' => 'setSortOrder',
        'surcharge' => 'setSurcharge'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'additional_shipping_cost' => 'getAdditionalShippingCost',
        'buyer_responsible_for_pickup' => 'getBuyerResponsibleForPickup',
        'buyer_responsible_for_shipping' => 'getBuyerResponsibleForShipping',
        'free_shipping' => 'getFreeShipping',
        'shipping_carrier_code' => 'getShippingCarrierCode',
        'shipping_cost' => 'getShippingCost',
        'shipping_service_code' => 'getShippingServiceCode',
        'ship_to_locations' => 'getShipToLocations',
        'sort_order' => 'getSortOrder',
        'surcharge' => 'getSurcharge'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }


    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->setIfExists('additional_shipping_cost', $data ?? [], null);
        $this->setIfExists('buyer_responsible_for_pickup', $data ?? [], null);
        $this->setIfExists('buyer_responsible_for_shipping', $data ?? [], null);
        $this->setIfExists('free_shipping', $data ?? [], null);
        $this->setIfExists('shipping_carrier_code', $data ?? [], null);
        $this->setIfExists('shipping_cost', $data ?? [], null);
        $this->setIfExists('shipping_service_code', $data ?? [], null);
        $this->setIfExists('ship_to_locations', $data ?? [], null);
        $this->setIfExists('sort_order', $data ?? [], null);
        $this->setIfExists('surcharge', $data ?? [], null);
    }

    /**
    * Sets $this->container[$variableName] to the given data or to the given default Value; if $variableName
    * is nullable and its value is set to null in the $fields array, then mark it as "set to null" in the
    * $this->openAPINullablesSetToNull array
    *
    * @param string $variableName
    * @param array  $fields
    * @param mixed  $defaultValue
    */
    private function setIfExists(string $variableName, array $fields, $defaultValue): void
    {
        if (self::isNullable($variableName) && array_key_exists($variableName, $fields) && is_null($fields[$variableName])) {
            $this->openAPINullablesSetToNull[] = $variableName;
        }

        $this->container[$variableName] = $fields[$variableName] ?? $defaultValue;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets additional_shipping_cost
     *
     * @return \MyBit\Ebay\Account\Model\Amount|null
     */
    public function getAdditionalShippingCost()
    {
        return $this->container['additional_shipping_cost'];
    }

    /**
     * Sets additional_shipping_cost
     *
     * @param \MyBit\Ebay\Account\Model\Amount|null $additional_shipping_cost additional_shipping_cost
     *
     * @return self
     */
    public function setAdditionalShippingCost($additional_shipping_cost)
    {
        if (is_null($additional_shipping_cost)) {
            throw new \InvalidArgumentException('non-nullable additional_shipping_cost cannot be null');
        }
        $this->container['additional_shipping_cost'] = $additional_shipping_cost;

        return $this;
    }

    /**
     * Gets buyer_responsible_for_pickup
     *
     * @return bool|null
     */
    public function getBuyerResponsibleForPickup()
    {
        return $this->container['buyer_responsible_for_pickup'];
    }

    /**
     * Sets buyer_responsible_for_pickup
     *
     * @param bool|null $buyer_responsible_for_pickup This field should be included and set to <code>true</code> for a motor vehicle listing if it will be the buyer's responsibility to pick up the purchased motor vehicle after full payment is made. <br><br>This field is only applicable to motor vehicle listings. In the majority of motor vehicle listings, the seller does make the buyer responsible for pickup or shipment of the vehicle. <br><br>This field is returned if set.<br><br><b>Default</b>: false
     *
     * @return self
     */
    public function setBuyerResponsibleForPickup($buyer_responsible_for_pickup)
    {
        if (is_null($buyer_responsible_for_pickup)) {
            throw new \InvalidArgumentException('non-nullable buyer_responsible_for_pickup cannot be null');
        }
        $this->container['buyer_responsible_for_pickup'] = $buyer_responsible_for_pickup;

        return $this;
    }

    /**
     * Gets buyer_responsible_for_shipping
     *
     * @return bool|null
     */
    public function getBuyerResponsibleForShipping()
    {
        return $this->container['buyer_responsible_for_shipping'];
    }

    /**
     * Sets buyer_responsible_for_shipping
     *
     * @param bool|null $buyer_responsible_for_shipping This field should be included and set to <code>true</code> for a motor vehicle listing if it will be the buyer's responsibility to arrange for shipment of a purchased motor vehicle after full payment is made. <br><br>This field is only applicable to motor vehicle listings. In the majority of motor vehicle listings, the seller does make the buyer responsible for pickup or shipment of the vehicle. <br><br>This field is returned if set.<br><br><b>Default</b>: false
     *
     * @return self
     */
    public function setBuyerResponsibleForShipping($buyer_responsible_for_shipping)
    {
        if (is_null($buyer_responsible_for_shipping)) {
            throw new \InvalidArgumentException('non-nullable buyer_responsible_for_shipping cannot be null');
        }
        $this->container['buyer_responsible_for_shipping'] = $buyer_responsible_for_shipping;

        return $this;
    }

    /**
     * Gets free_shipping
     *
     * @return bool|null
     */
    public function getFreeShipping()
    {
        return $this->container['free_shipping'];
    }

    /**
     * Sets free_shipping
     *
     * @param bool|null $free_shipping This field is included and set to <code>true</code> if the seller offers a free shipping option to the buyer. <br><br>This field can only be included and set to <code>true</code> for the first domestic shipping service option specified in the <b>shippingServices</b> container (it is ignored if set for subsequent shipping services or for any international shipping service option). <br><br>The first specified shipping service option has a <b>sortOrder</b> value of <code>1</code> or if the <b>sortOrderId</b> field is not used, it is the shipping service option that's specified first in the <b>shippingServices</b> container.<br><br>This container is returned if set.
     *
     * @return self
     */
    public function setFreeShipping($free_shipping)
    {
        if (is_null($free_shipping)) {
            throw new \InvalidArgumentException('non-nullable free_shipping cannot be null');
        }
        $this->container['free_shipping'] = $free_shipping;

        return $this;
    }

    /**
     * Gets shipping_carrier_code
     *
     * @return string|null
     */
    public function getShippingCarrierCode()
    {
        return $this->container['shipping_carrier_code'];
    }

    /**
     * Sets shipping_carrier_code
     *
     * @param string|null $shipping_carrier_code This field sets/indicates the shipping carrier, such as <code>USPS</code>, <code>FedEx</code>, or <code>UPS</code>. Although this field uses the <b>string</b> type, the seller must pass in a pre-defined enumeration value here. <br><br>For a full list of shipping carrier enum values for a specified eBay marketplace, the <a href=\"/devzone/xml/docs/reference/ebay/GeteBayDetails.html\">GeteBayDetails</a> call of the <b>Trading API</b> can be used, and the <b>DetailName</b> field's value should be set to <code>ShippingCarrierDetails</code>. The enum values for each shipping carriers can be found in each <b>ShippingCarrierDetails.ShippingCarrier</b> field in the response payload.<br><br> This field is actually optional, as the shipping carrier is also tied into the <b>shippingServiceCode</b> enum value, and that field is required for every specified shipping service option.<br><br>This field is returned if set.
     *
     * @return self
     */
    public function setShippingCarrierCode($shipping_carrier_code)
    {
        if (is_null($shipping_carrier_code)) {
            throw new \InvalidArgumentException('non-nullable shipping_carrier_code cannot be null');
        }
        $this->container['shipping_carrier_code'] = $shipping_carrier_code;

        return $this;
    }

    /**
     * Gets shipping_cost
     *
     * @return \MyBit\Ebay\Account\Model\Amount|null
     */
    public function getShippingCost()
    {
        return $this->container['shipping_cost'];
    }

    /**
     * Sets shipping_cost
     *
     * @param \MyBit\Ebay\Account\Model\Amount|null $shipping_cost shipping_cost
     *
     * @return self
     */
    public function setShippingCost($shipping_cost)
    {
        if (is_null($shipping_cost)) {
            throw new \InvalidArgumentException('non-nullable shipping_cost cannot be null');
        }
        $this->container['shipping_cost'] = $shipping_cost;

        return $this;
    }

    /**
     * Gets shipping_service_code
     *
     * @return string|null
     */
    public function getShippingServiceCode()
    {
        return $this->container['shipping_service_code'];
    }

    /**
     * Sets shipping_service_code
     *
     * @param string|null $shipping_service_code This field sets/indicates the domestic or international shipping service option, such as <code>USPSPriority</code>, <code>FedEx2Day</code>, or <code>UPS3rdDay</code>. Although this field uses the <b>string</b> type, the seller must pass in a pre-defined enumeration value here. <br><br>For a full list of shipping service option enum values for a specified eBay marketplace, the <a href=\"/devzone/xml/docs/reference/ebay/GeteBayDetails.html\">GeteBayDetails</a> call of the <b>Trading API</b> can be used, and the <b>DetailName</b> field's value should be set to <code>ShippingServiceDetails</code>. <br><br>The enum values for each shipping service option can be found in each <b>ShippingServiceDetails.ShippingService</b> field in the response payload. The seller must make sure that the shipping service option is still valid, which is indicated by a <code>true</code> value in the corresponding <b>ValidForSellingFlow</b> boolean field. International shipping service options are typically returned at the top of the response payload, and are indicated by an <b>InternationalService</b> boolean field that reads <code>true</code>. <br><br>The <b>InternationalService</b> boolean field is not returned at all for domestic shipping service options. <br><br> This field is required for every specified shipping service option.<br><br>This field is returned if set.
     *
     * @return self
     */
    public function setShippingServiceCode($shipping_service_code)
    {
        if (is_null($shipping_service_code)) {
            throw new \InvalidArgumentException('non-nullable shipping_service_code cannot be null');
        }
        $this->container['shipping_service_code'] = $shipping_service_code;

        return $this;
    }

    /**
     * Gets ship_to_locations
     *
     * @return \MyBit\Ebay\Account\Model\RegionSet|null
     */
    public function getShipToLocations()
    {
        return $this->container['ship_to_locations'];
    }

    /**
     * Sets ship_to_locations
     *
     * @param \MyBit\Ebay\Account\Model\RegionSet|null $ship_to_locations ship_to_locations
     *
     * @return self
     */
    public function setShipToLocations($ship_to_locations)
    {
        if (is_null($ship_to_locations)) {
            throw new \InvalidArgumentException('non-nullable ship_to_locations cannot be null');
        }
        $this->container['ship_to_locations'] = $ship_to_locations;

        return $this;
    }

    /**
     * Gets sort_order
     *
     * @return int|null
     */
    public function getSortOrder()
    {
        return $this->container['sort_order'];
    }

    /**
     * Sets sort_order
     *
     * @param int|null $sort_order The integer value set in this field controls the order of the corresponding domestic or international shipping service option in the View Item and Checkout pages. <br><br>Sellers can specify up to four domestic shipping services (in four separate <b>shippingService</b> containers), so valid values are 1, 2, 3, and 4. A shipping service option with a <b>sortOrder</b> value of <code>1</code> appears at the top of View Item and Checkout pages. Conversely, a shipping service option with a <b>sortOrder</b> value of <code>1</code> appears at the bottom of the list. <br><br>Sellers can specify up to five international shipping services (in five separate <b>shippingService</b> containers), so valid values for international shipping services are 1, 2, 3, 4, and 5. Similarly to domestic shipping service options, the <b>sortOrder</b> value of a international shipping service option controls the placement of that shipping service option in the View Item and Checkout pages. <br><br>If the <b>sortOrder</b> field is not supplied, the order of domestic and international shipping service options is determined by the order in which they are listed in the API call. <br><br><b>Min</b>: 1. <b>Max</b>: 4 (for domestic shipping service) or 5 (for international shipping service).
     *
     * @return self
     */
    public function setSortOrder($sort_order)
    {
        if (is_null($sort_order)) {
            throw new \InvalidArgumentException('non-nullable sort_order cannot be null');
        }
        $this->container['sort_order'] = $sort_order;

        return $this;
    }

    /**
     * Gets surcharge
     *
     * @return \MyBit\Ebay\Account\Model\Amount|null
     */
    public function getSurcharge()
    {
        return $this->container['surcharge'];
    }

    /**
     * Sets surcharge
     *
     * @param \MyBit\Ebay\Account\Model\Amount|null $surcharge surcharge
     *
     * @return self
     */
    public function setSurcharge($surcharge)
    {
        if (is_null($surcharge)) {
            throw new \InvalidArgumentException('non-nullable surcharge cannot be null');
        }
        $this->container['surcharge'] = $surcharge;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset): bool
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value): void
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset): void
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


