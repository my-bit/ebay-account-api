<?php
/**
 * SetPaymentPolicyResponseTest
 *
 * PHP version 7.4
 *
 * @category Class
 * @package  MyBit\Ebay\Account
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Account API
 *
 * The <b>Account API</b> gives sellers the ability to configure their eBay seller accounts, including the seller's policies (eBay business policies and seller-defined custom policies), opt in and out of eBay seller programs, configure sales tax tables, and get account information.  <br><br>For details on the availability of the methods in this API, see <a href=\"/api-docs/sell/account/overview.html#requirements\">Account API requirements and restrictions</a>.
 *
 * The version of the OpenAPI document: v1.9.2
 * Generated by: https://openapi-generator.tech
 * Generator version: 7.6.0
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace MyBit\Ebay\Account\Test\Model;

use PHPUnit\Framework\TestCase;

/**
 * SetPaymentPolicyResponseTest Class Doc Comment
 *
 * @category    Class
 * @description Complex type that that gets populated with a response containing a payment policy.
 * @package     MyBit\Ebay\Account
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class SetPaymentPolicyResponseTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "SetPaymentPolicyResponse"
     */
    public function testSetPaymentPolicyResponse()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "category_types"
     */
    public function testPropertyCategoryTypes()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "deposit"
     */
    public function testPropertyDeposit()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "description"
     */
    public function testPropertyDescription()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "full_payment_due_in"
     */
    public function testPropertyFullPaymentDueIn()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "immediate_pay"
     */
    public function testPropertyImmediatePay()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "marketplace_id"
     */
    public function testPropertyMarketplaceId()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "payment_instructions"
     */
    public function testPropertyPaymentInstructions()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "payment_methods"
     */
    public function testPropertyPaymentMethods()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "payment_policy_id"
     */
    public function testPropertyPaymentPolicyId()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "warnings"
     */
    public function testPropertyWarnings()
    {
        // TODO: implement
        self::markTestIncomplete('Not implemented');
    }
}
