# # SellerEligibilityMultiProgramResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**advertising_eligibility** | [**\MyBit\Ebay\Account\Model\SellerEligibilityResponse[]**](SellerEligibilityResponse.md) | An array of response fields that define the seller eligibility for eBay advertising programs. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
