# # Deposit

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | [**\MyBit\Ebay\Account\Model\Amount**](Amount.md) |  | [optional]
**due_in** | [**\MyBit\Ebay\Account\Model\TimeDuration**](TimeDuration.md) |  | [optional]
**payment_methods** | [**\MyBit\Ebay\Account\Model\PaymentMethod[]**](PaymentMethod.md) | This array is no longer applicable and should not be used since eBay now manages the electronic payment options available to buyers to pay the deposit. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
